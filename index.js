const
    fs = require('fs'),
    util = require('util');

const
    Discord = require('discord.js'),
    Datastore = require('nedb-promises');

/**
 * @class Datastore
 * @param {string} token
 * @param {string} [guildId]
 * @param {string} [channelId]
 * @param {string} [userId]
 * @param {string} [messageId]
 */
module.exports = function(
    token,
    guildId,
    channelId,
    userId,
    messageId
){
    const
        client = new Discord.Client(),
        filename = `g${guildId || ''}_c${channelId || ''}_u${userId || ''}_m${messageId || ''}_t${Date.now()}.db`,
        database = Datastore.create(filename);

    let message;

    /**
     * @method loadDatabase
     * @returns {Promise<string>} messageId
     */
    this.loadDatabase = async () => new Promise((resolve, reject) => {
        const doLoadDatabase = async () => {
            let channel;
            if(guildId && channelId)
                channel = client.guilds.cache.get(guildId).channels.cache.get(channelId);
            else if(userId)
                channel = await (await client.users.fetch(userId)).createDM();
            if(messageId){
                message = await channel.messages.fetch(messageId);
                await util.promisify(fs.writeFile)(filename, message.content, 'utf8');
            }
            else
                message = await channel.send('_ _');
            await database.loadDatabase();

            return message.id;
        };

        if(!client.user){
            client.login(token);
            client.once('ready', () => doLoadDatabase().then(resolve).catch(reject));
        }
        else
            doLoadDatabase().then(resolve).catch(reject);
    });

    /**
     * @method deleteDatabase
     * @param {boolean} [isDeletingLocally=true]
     * @param {boolean} [isDeletingRemotely=false]
     * @returns {Promise<void>}
     */
    this.deleteDatabase = async (isDeletingLocally = true, isDeletingRemotely = false) => {
        if(isDeletingLocally)
            await util.promisify(fs.unlink)(filename);
        if(isDeletingRemotely)
            await message.delete();
    };

    this.find = (...args) => database.find(...args);
    this.findOne = (...args) => database.findOne(...args);
    this.count = (...args) => database.count(...args);

    {
        const compactDatafile = () => new Promise(resolve => {
            database.persistence.compactDatafile();
            database.once('compactionDone', resolve);
        });

        const updateMessage = async () => {
            const doUpdateMessage = async () => await message.edit(await util.promisify(fs.readFile)(filename, 'utf8'));
            try {
                await doUpdateMessage();
            }
            catch(_){
                await compactDatafile();
                await doUpdateMessage();
            }
        };

        this.insert = async (...args) => {
            const res = await database.insert(...args);
            await updateMessage();
            return res;
        };

        this.update = async (...args) => {
            const res = await database.update(...args);
            await updateMessage();
            return res;
        };

        this.remove = async (...args) => {
            const res = await database.remove(...args);
            await updateMessage();
            return res;
        };

        this.ensureIndex = async (...args) => {
            await database.ensureIndex(...args);
            await updateMessage();
        };

        this.removeIndex = async (...args) => {
            await database.removeIndex(...args);
            await updateMessage();
        };

        this.persistence = {
            compactDatafile: async () => {
                await compactDatafile();
                await updateMessage();
            }
        };
    }
};