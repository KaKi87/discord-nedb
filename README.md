# Discord NeDB

NeDB-based database hosted on Discord.

## Installation

From [npm](https://www.npmjs.com/package/discord-nedb) :

- `yarn add discord-nedb`

or

- `npm install discord-nedb`

## Built with

- [`discord.js`](https://github.com/discordjs/discord.js) : Discord API wrapper
- [`nedb-promises`](https://github.com/bajankristof/nedb-promises) : NeDB wrapper with Promises
<br>(based on [`nedb`](https://github.com/louischatriot/nedb) : JS database)
  
## Usage

### Initialization

```js
const Datastore = require('discord-nedb');

const database = new Datastore(
    'token',
    'guildId',
    'channelId',
    'userId',
    'messageId'
);

const messageId = await database.loadDatabase();
```

- The `token` parameter is a required bot token
- Either provide `guildId` + `channelId` or `userId`
- When `messageId` is *undefined*, a new message will be created
- Always call `loadDatabase`, which will return `messageId`, especially useful at first initialization

### Data manimulation

Methods provided by `nedb-promises` (check [`Datastore` docs](https://github.com/bajankristof/nedb-promises/blob/master/docs.md#Datastore)) :

#### Read

- `find`
- `findOne`
- `count`

#### Write

- `update`
- `remove`
- `ensureIndex`
- `removeIndex`

### Database compaction

```js
await database.persistence.compactDatafile();
```

Database compaction is automatically called when the message exceeds Discord's maximum limit.

If compaction doesn't free enough space for additional data, an error will be thrown.

### Database deletion

```js
await database.deleteDatabase(
    isDeletingLocally,
    isDeletingRemotely
)
```

- When `isDeletingLocally` is `true` (default), the local database file is deleted.
- When `isDeletingRemotely` is `true` (not default), the Discord message is deleted.

## Changelog

- `0.1.0` (2020-12-17) • Initial release

## License

This project is released under the MIT license.